program tetrplus;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes
  { you can add units after this };

{$R *.res}

type
     TColorArray = Array[1..7] of Byte;

     THighScore = record
       msName  : String;
       miScore : Integer;
     end;

Const caiColorful   : TColorArray  = ($0f,$0e,$0d,$0c,$0b,$0a,$09) ;
      csHighScores  = 'highscores';
      //caiMonochrome : TColorArray  = ($1f,$1f,$1f,$1f,$1f,$1f,$1f);
     // Ch                :ch_ar    =($db,$b0,$b1,$b2,$db,$b0,$b1,$b2,$db,$b0);
      //                            219,176,177,178,219,176,177,178,219,176
      //cw:byte=11;
      //fon:byte=0;
      cbtColor = $0B
      f1:Byte=$1f;
      f2:Byte=$7f;
      File_Name:String='Tetris_p.dat';
      Turbo1=3000;
      Turbo2=150;
      Nturbo1=1500;
      Nturbo2=50;

procedure setColor(pbtColor : Byte);
begin
  TextColor(pbtColor mod 16);
  TextBackground(pbtColor div 16);
end;


procedure writeAtPos(pbtX,pbtY,pbtColor : Byte; psLine : String);
begin
  gotoXY(pbtX,pbtY);
  setColor(pbtColor);
  write(psLine);
end;


function loadHighScores : Boolean;
var ltFile : Text;
begin
  Assign(ltFile,File_Name);
  Reset(file_dat);
  if IOResult=0 then begin {*1,if}
    i:=1;
    While not eof(file_dat) do begin {*2,while}
      Read(file_dat,Resultat[i]);
      i:=i+1;
    end;{*2,while}
    Close(file_dat);
  end {*1,if}
  else begin {*1,else}
    rewr:=true;
    for i:=1 to 20 do begin {*2,for}
      with resultat[i] do begin {*3,with}
        n1:=84;n2:=69;n3:=84;n4:=82;res:=0;
      end; {*3,with}
    end;{*2,for}
  end;{*1,else}
end;

{---------------------------------------}
var typ_do,Typ          :Byte;
    rest,x,y,x1,y1,i,j,b:Byte;
    pas,scor,ok,summa,sstr,cstr,sc2,sfig :Word;
    Pol,pol1,level,pad  :byte;
    pl                  :Byte;
    stop_f              :Boolean;
    //cur_c
    laiColorSet         : TColorArray;
    parm                :String;
    Sound_f             :Boolean;
    next                :Boolean;
    Resultat            :High_arr;
    c_r,ic              :byte;
    Arrow               :Boolean;
    PageC               :Byte;
    kraka,kraka_d       :zyabla;
    kzyab               :Boolean;
    _bw,_t              :Boolean;
    pro                 :byte;
    batch               :text;
    k1,k2               :Integer;
    name                :string;
    GameOver            :Boolean;
    rewr                :Boolean;
    prow_f              :Boolean;
    restt               :string;
    pro_f               :Boolean;
    dwn_f               :Boolean;
    krs                 :krak_s;
    disd                :boolean;
    {---------------------------------------}
begin
  cursorOff;
  laiColorSet:=caiColorFul;
  (*
  if paramCount>0 then begin

    for i:=1 to ParamCount do begin {*2,for}
      parm:=ParamStr(i);
      For j:=1 to length(parm) do parm[j]:=Upcase(parm[j]);
      if parm='/BW' then begin laiColorSet:=mono;_bw:=true;end;
      if parm='/S'  then Sound_f:=False;
      if parm='/T'  then begin Scor:=NTurbo1;sc2:=scor;pad:=Nturbo2;_t:=True;end;
      if parm='/H'  then begin
        //TextColor(15);TextBackground(1);
        setColor($1F);
        Window(1,1,80,25);ClrScr;
        Writeln(#13,#10,'Tetris-plus ver 1.0 written by Џ е®¬Ґ­Є®ў Ђ.Џ. ‘” Њќ€ Є д. Ђ’ ‘¬®«Ґ­бЄ 1993');
      help;halt;end;
      If parm='/?'  then begin {*3,if}
        //TextColor(15);TextBackground(1);
        setColor($1F);
        Window(1,1,80,25);
        ClrScr;
        Writeln(#13,#10,'Tetris-plus ver 1.0 written by Џ е®¬Ґ­Є®ў Ђ.Џ. ‘” Њќ€ Є д. Ђ’ ‘¬®«Ґ­бЄ 1993');
        Writeln('Parameters : /BW - Black&White mode');
        Writeln('             /S  - No sound        ');
        Writeln('             /T  - For non-turbo computer');
        Writeln('             /?  - This Help       ');
        Writeln('             /H  - Full Help       ');
        Writeln('             /G  - Generation BATCH-file');
        halt;
      end;{*3,if}
      if parm='/G' then begin {*3,if}
        assign (batch,'run_tetr.bat');
        {$i-}
        rewrite (batch);
        {$i+}
        if IOResult<>0 then begin {*4,if}
          write(#13,#10,'Disk write protect-operation aborted !!');
          halt;
        end; {*4,if}
        writeln(batch,'@ echo off');
        writeln(batch,'echo TETRIS-PLUS');
        write(batch,'tetris_p.exe ');
        if _bw then write(batch,'/BW ');
        if not Sound_f then write(batch,'/S ');
        if _t then write(batch,'/T ');
        writeln(batch);
        write(batch,#26);
        Close(batch);
        halt;
      end;{*3,if}
    end; {*2,for}
  end;{*1,if}
  *)
  {-------------------------------------}
  k1:=1;k2:=1;
  clrScr;
  {-------------------------------------}
  loadResults;
  repeat; {*1,rep}

    setColor(cbtColor);
    clrScr;
    Draw_Kol;

    aw(0,1,18,22,$1e,$d5,$cd,$b8,$b3,$be,$cd,$d4,$b3);
    aw(57,8,76,22,$1e,$d5,$cd,$b8,$b3,$be,$cd,$d4,$b3);
    SetXY(3,2);Write(' High Score : ');
    For i:=1 to 20 do begin {*2,for}
      With resultat[i] do begin {*3,with}
        restt:=strn(res,5);
        SetXY(3,2+i);write(chr(n1)+chr(n2)+chr(n3)+chr(n4),'......',strn(res,5));
      end; {*3,with}
    end;
    Enter_string(name,' Enter your name : ',28,14,4,$1f,$75);
    //TextColor(14);TextBackGround(1);
    setColor($1E);
    SetXY(28,14);Write('                       ');
    level:=0;cstr:=1;sstr:=0;sfig:=0;scor:=Turbo1;sc2:=scor;pad:=turbo2;
    summa:=0;ok:=0;kzyab:=false;pas:=0;b:=0;Typ_do:=random(7)+1;
    if typ_do=9 then kz_init(kraka_d,krs,ch,level);
    randomize;
    pol1:=random(3)+1;next:=true;
    Repeat; {*1,rep}
      prow_f:=false;pro_f:=false;dwn_f:=false;
      pro:=((level+2) div 2)+1;disd:=false;
      Typ:=Typ_do;pol:=pol1;Typ_do:=Random(9)+1;Pol1:=Random(3)+1;kraka:=kraka_d;
      if typ_do=9 then kz_init(kraka_d,krs,ch,level);
      if typ=8 then if SOUND_F then music(4);
      if typ=9 then begin {*2,if}
        if sound_f then Music(2);
        kzyab:=true;
      end {*2,if}
      else kzyab:=false;
      inc(sfig);
      ok:=22;
      rest:=0;x:=40;y:=2;x1:=x;scor:=sc2;pas:=0;y1:=y;
      Out_cnt(level,cw,fon,sstr,summa,sfig,name,sound_f,_bw);
      if not kzyab then begin {*2,if}
        Draw_Figure(rest,x,y,typ,pol,ch,laiColorSet,level);
        Shad_Figure(cw,fon,typ,pol,x);
      end {*2,if}
      else begin {*2,else}
        draw_kraka(kraka,rest,x,y,typ,pol,ch,laiColorSet,level,f1);
        shad_kraka(kraka,krs,cw,fon,typ,pol,x);
      end;{*2,else}
      stop_f:=false;
      if next then begin {*2,if}
        Window(65,3,73,6);
        //TextColor(cw);TextBackGround(fon);
        setColor(cbtColor);

        ClrScr;Window(1,1,80,25);
        if typ_do<>9 then Clear_Figure( rest,68,4,typ_do,pol1,f2)
        else Clear_kraka(kraka_d,rest,68,4,typ_do,pol1,f2,ch,level,laiColorSet);
      end {*2,if}
      else begin {*2,else}
        Window(65,3,70,6);
        //TextColor(cw);TextBackGround(fon);
        setColor(cbtColor);
        ClrScr;Window(1,1,80,25);
      end; {*2,else}
      Arrow:=True;
      While rest=0 do begin {*2,while}
        if pas>=scor then begin {*3,if}
          pas:=0;
          if scor<>pad then ok:=ok-1;
          if rest=0 then begin {*4,if}
            y1:=y;x1:=x;
            if y=25 then
              write;
            inc(y);
            if not kzyab then begin {*5,if}
              Clear_Figure(rest,x1,y1,typ,pol,f1);
              Test_Figure(rest,x,y,typ,pol);
            end {*5,if}
            else begin {*5,else}
              Clear_kraka(kraka,rest,x1,y1,typ,pol,f1,ch,level,laiColorSet);
              test_kraka(kraka,rest,x,y,typ,pol,ch,level);
            end;{*5,else}
            if (typ=8) and (pro<>0) and (rest<>0) and (y<25) then begin {*5,if}
              dec(pro);
              rest:=0;
              pro_f:=true;
            end;{*5,if}
            if rest<>0 then begin {*5,if}
              Arrow:=False;
              SayXY(23,25,$B0,colbor,34);
              if not kzyab then Draw_Figure(rest,x1,y1,typ,pol,ch,laiColorSet,level)
              else Draw_kraka(kraka,rest,x1,y1,typ,pol,ch,laiColorSet,level,f1);
              rest:=10;
              if Game_Over then GameOver:=true else GameOver:=false;
              Prowal(cstr,sstr,sound_f,prow_f);
              summa:=summa+ok;disd:=true;
              Out_cnt(level,cw,fon,sstr,summa,sfig,name,sound_f,_bw);
              if cstr>10 then begin {*6,if}
                if level<9 then begin inc(level);if sound_f then Music(3);end;
                cstr:=1;
                sc2:=sc2-30*level*2;
              end{*6,if}
            end; {*5,if}
          end; {*4,if}
          if (not kzyab) then begin
            if (not prow_f) and (rest=0) then Draw_Figure(rest,x,y,typ,pol,ch,laiColorSet,level);
            if (typ=8) and (pro=0) and (pro_f) and (rest<>0) and not(disd) then begin
                Test_Figure(rest,x,y+1,typ,pol);
              pro_f:=false;
            end;
          end
          else begin
            if (not prow_f) and (rest=0) then begin
              Draw_kraka(kraka,rest,x,y,typ,pol,ch,laiColorSet,level,f1);
              Shad_Kraka(kraka,krs,cw,fon,typ,pol,x);
            end;
          end;
        end; {*3,if}
        if prow_f then rest:=10;
        inc(pas);
        if (typ=8) and (pro<>0) and (rest<>0) and (y<25) then rest:=0;
        If Arrow then begin {*3,if}
          if keypressed then begin {*4,if}
            x1:=x;y1:=y;
            b:=ord(readkey);
            if b=0 then b:=ord(readkey);
            if (b=32) or (b=80) then scor:=150;
            if (b=115) or (b=83) then if sound_f then begin
              sound_f:=false;
              Out_cnt(level,cw,fon,sstr,summa,sfig,name,sound_f,_bw);
            end
            else begin
              sound_f:=true;
              Out_cnt(level,cw,fon,sstr,summa,sfig,name,sound_f,_bw);
            end;
            if b=68 then begin {*5,if}
              //TextBackGround(0);TextColor(15);
              setColor($0F);
              ClrScr;
              cur_on;
              halt;
            end; {*5,if}
            if b=9 then begin {*5,if}
              //SetXY(36,25);TextColor(30);TextBackGround(1);Write(' Pause...');
              writeAtPos(36,25,$1F);
              b:=ord(readkey);if b=0 then b:=ord(readkey);
              //TextColor(cw);TextBackGround(fon);
              setColor(cbtColor);
              writeAtPos(23,25,$B0,colbor{,34});
              Shad_Figure(cw,fon,typ,pol,x);
            end; {*5,if}
            if (b=75) and (x>24) then x:=x-2;  {24,55}
            if (b=77) and (x<55) then x:=x+2;
            if b=27 then Pause(pagec);
            if b=49 then begin {*5,if}
              if next then begin {*6,if}
                next:=false;
                Window(65,3,70,6);
                //TextColor(cw);TextBackGround(fon);
                setColor(cbtColor);
                ClrScr;Window(1,1,80,25);
              end {*6,if}
              else begin {*6,else}
                Window(65,3,70,6);
                //TextColor(cw);TextBackGround(fon);
                setColor(cbtColor);
                ClrScr;Window(1,1,80,25);
                if typ_do<>9 then Clear_Figure(rest,68,4,typ_do,pol1,f2)
                else Clear_kraka (kraka_d,rest,68,4,typ_do,pol1,f2,ch,level,laiColorSet);
                next:=true;
              end; {*6,else}
            end; {*5,if}
            if b=72 then begin {*5,if}
              SayXY(23,25,$B0,colbor,34);
              if not kzyab then Clear_Figure(rest,x1,y1,typ,pol,f1)
              else Clear_kraka(kraka,rest,x1,y1,typ,pol,f1,ch,level,laiColorSet);
              pl:=pol;
              dec(pol);if pol<1 then pol:=4;
              if not kzyab then Test_Figure(rest,x,y,typ,pol)
              else test_kraka(kraka,rest,x,y,typ,pol,ch,level);
              if rest=0 then begin {*6,if}
                if not kzyab then begin {*7,if}
                  Draw_Figure(rest,x,y,typ,pol,ch,laiColorSet,level);
                  Shad_Figure(cw,fon,typ,pol,x);
                end {*7,if}
                else begin {*7,else}
                  Draw_Kraka(kraka,rest,x,y,typ,pol,ch,laiColorSet,level,f1);
                  Shad_Kraka(kraka,krs,cw,fon,typ,pol,x);
                end; {*7 else}
              end; {*6,if}
              if rest=10 then begin {*6,if}
                pol:=pl;
                rest:=0;
                if not kzyab then begin {*7,if}
                  Draw_Figure(rest,x,y,typ,pol,ch,laiColorSet,level);
                  Shad_Figure(cw,fon,typ,pol,x);
                end {*7,if}
                else begin  {*7,else}
                  Draw_kraka(kraka,rest,x,y,typ,pol,ch,laiColorSet,level,f1);
                  Shad_kraka(kraka,krs,cw,fon,typ,pol,x)
                end; {*7,else}
              end; {*6,if}
            end; {*5,if}
            if (x<>x1) or (y<>y1) then begin {*5,if}
              SayXY(23,25,$B0,colbor,34);
              if not kzyab then begin {*6,if}
                Clear_Figure(rest,x1,y1,typ,pol,f1);
                Test_Figure(rest,x,y,typ,pol);
              end {*6,if}
              else begin {*6,else}
                Clear_kraka(kraka,rest,x1,y1,typ,pol,f1,ch,level,laiColorSet);
                Test_kraka(kraka,rest,x,y,typ,pol,ch,level);
              end; {*6,else}
              if rest=0 then begin {*6,if}
                if not kzyab then begin {*7,if}
                  Draw_Figure(rest,x,y,typ,pol,ch,laiColorSet,level);
                  Shad_Figure(cw,fon,typ,pol,x);
                end {*7,if}
                else begin {*7,else}
                  Draw_kraka(kraka,rest,x,y,typ,pol,ch,laiColorSet,level,f1);
                  Shad_kraka(kraka,krs,cw,fon,typ,pol,x);
                end; {*7,else}
              end {*6,if}
              else begin {*6,else}
                x:=x1;
                rest:=0;
                if not kzyab then begin {*7,if}
                  Draw_Figure(rest,x,y,typ,pol,ch,laiColorSet,level);
                  Shad_Figure(cw,fon,typ,pol,x);
                end {*7,if}
                else begin {*7,else}
                  Draw_kraka(kraka,rest,x,y,typ,pol,ch,laiColorSet,level,f1);
                  Shad_kraka(kraka,krs,cw,fon,typ,pol,x);
                end; {*7,else}
              end; {*6,else}
            end; {*5,if}
          end; {*4,if}
        end; {*3,if}
      end; {*2,while}
    until GameOver; {*1,repeat}
    ic:=21;
    For i:=1 to 20 do begin
      With resultat[i] do begin
        if summa>res then dec(ic);
      end;
    end;
    For i:=20 downto ic+1 do begin
      resultat[i]:=resultat[i-1];
    end;
    With resultat[ic] do begin
        n1:=ord(name[1]);n2:=ord(name[2]);n3:=ord(name[3]);n4:=ord(name[4]);
        res:=summa;
    end;
    {$i-}
    Rewrite(file_dat);
    {$i+}
    if ioresult<>0 then begin
      SetXY(21,25);Write(' Disk write protect - program aborted.');
      halt;
    end
    else begin
      for i:=1 to 20 do write(file_dat,resultat[i]);
      close(file_dat);
    end;
    For i:=1 to 20 do begin {*2,for}
      With resultat[i] do begin {*3,with}
        restt:=strn(res,5);
        SetXY(3,2+i);write(chr(n1)+chr(n2)+chr(n3)+chr(n4),'......',strn(res,5));
      end; {*3,with}
    end;
  until End_Game;

  ClrScr;
end. {program}


